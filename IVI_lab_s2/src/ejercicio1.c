#include <GL/glut.h>
#include <stdio.h>


/*Variables*/
static int pulsado = 0; /* BLanco = 0m Color = 1*/
static int menu_id;


/*Función de callback del menu*/ 

void menu(int num){
  printf("Entro %d \n", num);
    if(num == 1){
    	pulsado = 1;
    }
    if(num == 2){
      pulsado = 0;
    }
  glutPostRedisplay();
} 

/*Función de callback del teclado*/ 
void teclado(unsigned char key, int x, int y) {
  switch (key) { 
    case 'c': 
      if(pulsado == 0){
        pulsado = 1;
      }else{
        pulsado = 0;
      }
      printf(" Pulsado: %d\n",pulsado);
      break;
  }
  glutPostRedisplay();
}


/* Función de renderizado */
void render () {
  /* Limpieza de buffers */
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  /* Carga de la matriz identidad */
  glLoadIdentity();
  /* Traslación */
  glTranslatef(0.0, 0.0, -4.0);

  /* Renderiza un triángulo blanco */
  if(pulsado == 0){
    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_TRIANGLES);
    glVertex3f(0.0, 1.0, 0.0);
    glVertex3f(-1.0, -1.0, 0.0);
    glVertex3f(1.0, -1.0, 0.0);
    glEnd();
  }
  else{
    /* Renderiza un triángulo  */
    glBegin(GL_TRIANGLES);
  
    /*Cambio el color*/
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(0.0, 1.0, 0.0);
  
    /*Cambio el color*/
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(-1.0, -1.0, 0.0);
    
    /*Cambio el color*/
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3f(1.0, -1.0, 0.0);
    glEnd();
  }


  

  /* Intercambio de buffers */
  glutSwapBuffers();
}

/*Crear el menu*/

void createMenu(void){           
    menu_id = glutCreateMenu(menu);
    glutAddMenuEntry("Cambiar a Color", 1);
    glutAddMenuEntry("Cambiar a Blanco", 2);     
    glutAttachMenu(GLUT_RIGHT_BUTTON);
} 

void resize (int w, int h) {
  /* Definición del viewport */
  glViewport(0, 0, w, h);

  /* Cambio a transform. vista */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  /* Actualiza el ratio ancho/alto */
  gluPerspective(50., w/(double)h, 1., 10.);

  /* Vuelta a transform. modelo */
  glMatrixMode(GL_MODELVIEW);
}

void init (void) {
  glEnable(GL_DEPTH_TEST);
}

int main(int argc, char *argv[]) {
  glutInit(&argc, argv);

  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(400, 400);
  glutInitWindowPosition(200, 200);

  glutCreateWindow("Hola Mundo con OpenGL!");

  init();

  /* Registro de funciones de retrollamada */
  glutDisplayFunc(render);
  glutReshapeFunc(resize);
  glutKeyboardFunc(teclado);
  createMenu();

  /* Bucle de renderizado */
  glutMainLoop();

  return 0;
}
