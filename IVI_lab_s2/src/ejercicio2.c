#include <GL/glut.h>

static GLint rotate = 0;
static int g=0;
static int start = 0; /*Ya ha empezado a rotar*/
static int rotating = 0; /* Estoy rotando =1 no estoy rotando = 0*/
static int axis = 0;/* 0 = X, 1 = Y , 2 = Z*/
static int turning = 0 ; /* 0 giro hacia la derecha, 1 giro hacia la izquierda*/

/*Función de callback del teclado*/ 
void teclado(unsigned char key, int x, int y) {
  switch (key) { 
    case 'x': 
      if(start == 0){
        start = 1;
        rotating = 1;
        g = 1;
      }
      axis=0;
      break;
    case 'y':
      axis=1;
      break;
    case 'z':
      axis=2;
      break;
    case 's':
      if(start == 1){
		if(rotating == 0){
			rotating = 1;
		}else{
			rotating = 0;
		}
      }

      break;
  }
  glutPostRedisplay();
}

void teclado_especial(int key, int x, int y){
  switch (key) {
    case GLUT_KEY_LEFT:
      turning = 1;
      break;

    case GLUT_KEY_RIGHT:
      turning = 0;
      break;

  }
}


void render () { 
  /* Limpieza de buffers */
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  /* Carga de la matriz identidad */
  glLoadIdentity();
  /* Posición de la cámara virtual (position, look, up) */
  gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
  
  /* En color blanco */
  glColor3f( 1.0, 1.0, 1.0 );  
  /* Renderiza la tetera */
  if(g != 0 && rotating == 1){
    if(turning == 0){
      g++;
      if(g >= 360){
        g=1;
      }
    }
    else{
      g--;
      if(g <= 0){
        g=359;
      }
    }
  }
  /*Rotacion segun el eje indicado*/
  if(axis==0){
    glRotatef(g, 1.0, 0.0, 0.0);
  }
  if(axis==1){
    glRotatef(g, 0.0, 1.0, 0.0);
  }
  if(axis==2){
    glRotatef(g, 0.0, 0.0, 1.0);
  }

  
  glutWireTeapot(1.5);

  /* Intercambio de buffers... Representation ---> Window */
  glutSwapBuffers();      
} 

void resize (int w, int h) { 
  /* Definición del viewport */
  glViewport(0, 0, w, h);

  /* Cambio a transform. vista */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  /* Actualiza el ratio ancho/alto */
  gluPerspective(50., w/(double)h, 1., 10.);

  /* Vuelta a transform. modelo */
  glMatrixMode(GL_MODELVIEW);
}

int main (int argc, char* argv[]) { 
  glutInit( &argc, argv ); 
  glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE ); 
  glutInitWindowSize(640, 480); 
  glutCreateWindow( "IVI - Sesion 2" ); 
  glEnable (GL_DEPTH_TEST);
  
  /* Registro de funciones de retrollamada */
  glutDisplayFunc(render); 
  glutReshapeFunc(resize); 
  glutKeyboardFunc(teclado);
  glutSpecialFunc(teclado_especial);
  glutIdleFunc(render);
  
  /* Bucle de renderizado */
  glutMainLoop();  
  
  return 0; 
} 
